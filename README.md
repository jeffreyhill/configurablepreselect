# Configurable Product Preselect Module

| Version | 0.1.0 |
| Author | Jeffrey Hill |

## Features

- In the configurable product editor in the Magento backend, this module adds an 
optional 'Default' selection column that allows a merchandiser to choose a 
pre-selected simple product.  
- When saved, the pre-selected product SKU will be saved as part of the 
configurable product's EAV record.
- On the configurable product's storefront PDP, the options associated with the
pre-selected product will be selected when the page loads, and the main image
of the pre-selected product will appear.
- Also on the configurable product's PDP, selecting one or more of the product's 
available attribute options will append a URL hash in the address bar.  This hash
represents each selected attribute's name and option value.  It can be copied to 
preselect a simple product variation on the configurable product PDP.  
For example: `https://www.storefront.com/sandals.html#?color=1&size=234` 
- On a storefront PLP product grid, selecting a swatch of any configurable product
will append a hash fragment representing the selected product option to its 
product's anchor URL.  Clicking on the name or the image of the product will load
the configurable product page with the pre-selected product option, as explained
above.
- When a configurable product page is requested and includes a product option hash
fragment, the media gallery images of the pre-selected product's configurable 
product will not be displayed.
- Appending the product option information using a hash fragment ensures that any 
additional  information conveyed in a query string of a PDP or PLP URL will not be 
interfered with.  This is a feature intended to preserve tracking information 
conveyed by UTM and marketing query strings.

## Setup

1. Drop into `app/code` directory.
2. Run `bin/magento setup:upgrade` and `bin/magento setup:di:compile`.
On installation, the setup script creates a new EAV attribute: 
`default_simple_product_sku`
3. Run `bin/magento setup:static-content:deploy` to include frontend script overrides.
4. Clear cache.

## Known Issues

- Because several installed modules converge on it, swatch JS overrides that the module
depends on are currently located in a theme file: 
`app/design/frontend/.../Magento_Swatches/web/js/swatch-renderer.js`