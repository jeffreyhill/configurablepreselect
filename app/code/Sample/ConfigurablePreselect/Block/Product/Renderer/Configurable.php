<?php
namespace Sample\ConfigurablePreselect\Block\Product\Renderer;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\ConfigurableProduct\Helper\Data;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use Magento\InventorySalesApi\Api\IsProductSalableInterface;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;
use Magento\Swatches\Model\Swatch;
use Magento\Swatches\Model\SwatchAttributesProvider;

/**
 * Swatch renderer block
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Configurable extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
    /**
     * Path to template file with swatch renderer.
     */
    const SWATCH_RENDERER_TEMPLATE = 'Sample_ConfigurablePreselect::product/view/renderer.phtml';

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var SwatchData
     */
    protected $swatchHelper;

    /**
     * @var Media
     */
    protected $swatchMediaHelper;

    /**
     * Indicate if product has one or more Swatch attributes
     *
     * @deprecated 100.1.0 unused
     *
     * @var boolean
     */
    protected $isProductHasSwatchAttribute;

    /**
     * @var SwatchAttributesProvider
     */
    private $swatchAttributesProvider;

    /**
     * @var ScopeConfigInterface $scopeConfig
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var WebsiteRepositoryInterface
     */
    protected $websiteRepository;

    /**
     * @var StockResolverInterface
     */
    protected $stockResolver;

    /**
     * @var IsProductSalableInterface
     */
    protected $isProductSalable;

    /**
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param EncoderInterface $jsonEncoder
     * @param Data $helper
     * @param CatalogProduct $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param SwatchData $swatchHelper
     * @param Media $swatchMediaHelper
     * @param StoreManagerInterface $storeManager
     * @param WebsiteRepositoryInterface $webSiteRepository
     * @param StockResolverInterface $stockResolver
     * @param IsProductSalableInterface $isProductSalable
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     * @param SwatchAttributesProvider|null $swatchAttributesProvider
     * @param UrlBuilder|null $imageUrlBuilder
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        Data $helper,
        CatalogProduct $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        SwatchData $swatchHelper,
        Media $swatchMediaHelper,
        StoreManagerInterface $storeManager,
        WebsiteRepositoryInterface $webSiteRepository,
        StockResolverInterface $stockResolver,
        IsProductSalableInterface $isProductSalable,
        ScopeConfigInterface $scopeConfig,
        array $data = [],
        SwatchAttributesProvider $swatchAttributesProvider = null,
        UrlBuilder $imageUrlBuilder = null
    ) {
        $this->storeManager = $storeManager;
        $this->websiteRepository = $webSiteRepository;
        $this->stockResolver = $stockResolver;
        $this->isProductSalable = $isProductSalable;
        $this->scopeConfig = $scopeConfig;
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $swatchHelper,
            $swatchMediaHelper,
            $data,
            $swatchAttributesProvider,
            $imageUrlBuilder
        );
    }

    public function getStoreConfig($name)
    {
        return $this->scopeConfig->getValue(
            $name,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return renderer template (override)
     *
     * Template for product with swatches is different from product without swatches
     *
     * @return string
     */
    protected function getRendererTemplate()
    {
        return $this->isProductHasSwatchAttribute() ?
            self::SWATCH_RENDERER_TEMPLATE : parent::CONFIGURABLE_RENDERER_TEMPLATE;
    }

    /**
     * Produce and return block's html output
     *
     * @return string
     * @since 100.2.0
     */
    public function toHtml()
    {
        $this->setTemplate(
            $this->getRendererTemplate()
        );
        return parent::toHtml();
    }

    /**
     * Returns JSON encoded, preselected option ID
     *
     * @return string
     */
    public function getPreselectOptionId()
    {
        $optionDatas = $this->getProduct()->getTypeInstance()->getConfigurableOptions($this->getProduct());

        $associated_option = [];
        foreach ($optionDatas as $opt => $optionData) {
            foreach ($optionData as $optionVal) {
                $associated_option[$optionVal['sku']][$optionVal['attribute_code']] = $optionVal['value_index'];
            }
        }

        if (array_key_exists($this->getProduct()->getDefaultSimpleProductSku(), $associated_option)) {
            $result_data = $associated_option[$this->getProduct()->getDefaultSimpleProductSku()];
        } else {
            $result_data = current($associated_option);
        }

        return $this->jsonEncoder->encode($result_data);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _getAdditionalConfig()
    {
        $result = [ 'productStocks' => [] ];
        $store = $this->storeManager->getStore();
        $websiteId = $store->getWebsiteId();
        $websiteCode = $this->websiteRepository->getById($websiteId)->getCode();
        foreach ($this->getAllowProducts() as $product) {
            $stockId = $this->stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $websiteCode)->getStockId();
            $result['productStocks'][ $product->getId() ] = (int)$this->isProductSalable->execute($product->getSku(), $stockId);
        }
        return $result;
    }
}
