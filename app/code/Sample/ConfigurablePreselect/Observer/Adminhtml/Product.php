<?php
namespace Sample\ConfigurablePreselect\Observer\Adminhtml;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Product implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    public $request;

    /**
     * @var SerializerInterface
     */
    public $serializer;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        SerializerInterface $serializer
    ) {
        $this->request = $request;
        $this->serializer = $serializer;
    }

    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($this->request->getPost('configurable-matrix-serialized')) {
            $simpleProducts = $this->serializer->unserialize($this->request->getPost('configurable-matrix-serialized'));
            $saveFlag = false;

            foreach ($simpleProducts as $simpleProduct) {
                if (array_key_exists('checked', $simpleProduct) && $simpleProduct['checked']) {
                    $product->setStoreId($product->getStoreId())
                        ->setData('default_simple_product_sku', $simpleProduct['sku'])
                        ->save();
                    $saveFlag = true;
                }
            }

            // NL-205 If no selection is made, select first product variation as default SKU
            if (!$saveFlag) {
                $simpleProduct = reset($simpleProducts);
                $product->setStoreId($product->getStoreId())
                    ->setData('default_simple_product_sku', $simpleProduct['sku'])
                    ->save();
            }
        }
    }
}
