<?php
namespace Sample\ConfigurablePreselect\Ui\Component;

class Form extends \Magento\Ui\Component\Form
{
    /**
     * {@inheritdoc}
     */
    public function getDataSourceData()
    {
        $dataSource = [];

        $id = $this->getContext()->getRequestParam($this->getContext()->getDataProvider()->getRequestFieldName(), null);
        $filter = $this->filterBuilder->setField($this->getContext()->getDataProvider()->getPrimaryFieldName())
            ->setValue($id)
            ->create();
        $this->getContext()->getDataProvider()
            ->addFilter($filter);

        $data = $this->getContext()->getDataProvider()->getData();

        /* START configurable-matrix */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($objectManager->get('Magento\Framework\Registry')->registry('current_product')) {
            $product = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
            if ($product->getTypeId() ==  'configurable') {
                foreach ($data as $key_id => $data_value) {
                    if (array_key_exists('configurable-matrix', $data_value)) {
                        foreach ($data_value['configurable-matrix'] as $configurableMatrixKey => $simpleProduct) {
                            $data[$key_id]['configurable-matrix'][$configurableMatrixKey]['default_value'] = $simpleProduct['sku'];
                            if ($simpleProduct['sku'] == $product->getDefaultSimpleProductSku()) {
                                $data[$key_id]['configurable-matrix'][$configurableMatrixKey]['checked'] = 1;
                            } else {
                                $data[$key_id]['configurable-matrix'][$configurableMatrixKey]['checked'] = 0;
                            }
                        }
                    }
                }
            }
        }
        /* END configurable-matrix */

        if (isset($data[$id])) {
            $dataSource = [
                'data' => $data[$id]
            ];
        } elseif (isset($data['items'])) {
            foreach ($data['items'] as $item) {
                if ($item[$item['id_field_name']] == $id) {
                    $dataSource = ['data' => ['general' => $item]];
                }
            }
        }
        return $dataSource;
    }
}
